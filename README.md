### A ci-toolbox Docker Image with 

ARG TERRAFORM_VERSION=0.11.5  
ARG PACKER_VERSION=1.2.1  
ARG ANSIBLE_VERSION=2.3.1.0  
ARG ANSIBLE_LINT_VERSION=3.4.13  
ARG TERRAFORM_PROVIDER_ARCHIVE_VERSION=1.0.3  
ARG TERRAFORM_PROVIDER_AWS_VERSION=1.13.0  
ARG TERRAFORM_PROVIDER_TEMPLATE_VERSION=1.0.0  
ARG TERRAFORM_PROVIDER_TERRAFORM_VERSION=1.0.2  
ARG PYWINRM_VERSION=0.2.2  

Local terraform plugin repository at /usr/lib/custom-terraform-plugins if you'd like to omit downloading from releases.hashicorp every time you run terraform init

#### Where to get
https://hub.docker.com/r/gbvanrenswoude/ci-toolbox/