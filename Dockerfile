FROM alpine:latest

ARG AWS_DEFAULT_REGION=eu-central-1
ARG TERRAFORM_VERSION=0.12.24
ARG TERRAFORM_PROVIDER_ARCHIVE_VERSION=1.3.0
ARG TERRAFORM_PROVIDER_AWS_VERSION=2.54.0
ARG TERRAFORM_PROVIDER_TEMPLATE_VERSION=2.1.2
ARG TERRAFORM_PROVIDER_TERRAFORM_VERSION=1.0.2
ARG TERRAFORM_PROVIDER_TLS_VERSION=2.1.1
ARG TERRAFORM_PROVIDER_EXTERNAL_VERSION=1.2.0
ARG PACKER_VERSION=1.5.4
ARG ANSIBLE_VERSION=2.9.0
ARG ANSIBLE_LINT_VERSION=3.4.23
ARG SLS_VERSION=1.60.5

# All APK packages
RUN apk add --no-cache --upgrade \
        expect \
        bash \
        bind-tools \
        build-base \
        ca-certificates \
        coreutils \
        curl \
        gcc \
        git \
        jq \
        json-glib-dev \
        libc6-compat \
        libffi-dev \
        libressl-dev \
        make \
        musl-dev \
        nodejs \
        npm \
        openldap-dev \
        openssh-client \
        python3-dev \
        sed \
        zip && \
        update-ca-certificates

RUN pip3 install --upgrade \
        pip \
        setuptools && \
    pip3 install --upgrade \
        "ansible-lint==${ANSIBLE_LINT_VERSION}" \
        "ansible==${ANSIBLE_VERSION}" \
        autopep8 \
        aws-sam-cli \
        awscli \
        botocore \
        boto3 \
        c7n-org \
        dnspython \
        moto \
        python-ldap \
        sceptre \
        pyyaml \
        terraform_external_data

# Downloaded from https://releases.hashicorp.com/terraform-provider-aws/
RUN curl -o terraform_${TERRAFORM_VERSION}_linux_amd64.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    curl -o packer_${PACKER_VERSION}_linux_amd64.zip https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip && \
    curl -o terraform-provider-aws_${TERRAFORM_PROVIDER_AWS_VERSION}_linux_amd64.zip https://releases.hashicorp.com/terraform-provider-aws/${TERRAFORM_PROVIDER_AWS_VERSION}/terraform-provider-aws_${TERRAFORM_PROVIDER_AWS_VERSION}_linux_amd64.zip && \
    curl -o terraform-provider-terraform_${TERRAFORM_PROVIDER_TERRAFORM_VERSION}_linux_amd64.zip https://releases.hashicorp.com/terraform-provider-terraform/${TERRAFORM_PROVIDER_TERRAFORM_VERSION}/terraform-provider-terraform_${TERRAFORM_PROVIDER_TERRAFORM_VERSION}_linux_amd64.zip && \
    curl -o terraform-provider-template_${TERRAFORM_PROVIDER_TEMPLATE_VERSION}_linux_amd64.zip https://releases.hashicorp.com/terraform-provider-template/${TERRAFORM_PROVIDER_TEMPLATE_VERSION}/terraform-provider-template_${TERRAFORM_PROVIDER_TEMPLATE_VERSION}_linux_amd64.zip && \
    curl -o terraform-provider-archive_${TERRAFORM_PROVIDER_ARCHIVE_VERSION}_linux_amd64.zip https://releases.hashicorp.com/terraform-provider-archive/${TERRAFORM_PROVIDER_ARCHIVE_VERSION}/terraform-provider-archive_${TERRAFORM_PROVIDER_ARCHIVE_VERSION}_linux_amd64.zip && \
    curl -o terraform-provider-tls_${TERRAFORM_PROVIDER_TLS_VERSION}_linux_amd64.zip https://releases.hashicorp.com/terraform-provider-tls/${TERRAFORM_PROVIDER_TLS_VERSION}/terraform-provider-tls_${TERRAFORM_PROVIDER_TLS_VERSION}_linux_amd64.zip && \
    curl -o terraform-provider-external_${TERRAFORM_PROVIDER_EXTERNAL_VERSION}_linux_amd64.zip https://releases.hashicorp.com/terraform-provider-external/${TERRAFORM_PROVIDER_EXTERNAL_VERSION}/terraform-provider-external_${TERRAFORM_PROVIDER_EXTERNAL_VERSION}_linux_amd64.zip && \
    mkdir /usr/lib/custom-terraform-plugins && \
    unzip packer_${PACKER_VERSION}_linux_amd64.zip -d /usr/local/bin/ && \
    unzip terraform-provider-aws_${TERRAFORM_PROVIDER_AWS_VERSION}_linux_amd64.zip -d /usr/lib/custom-terraform-plugins && \
    unzip terraform-provider-terraform_${TERRAFORM_PROVIDER_TERRAFORM_VERSION}_linux_amd64.zip -d /usr/lib/custom-terraform-plugins && \
    unzip terraform-provider-template_${TERRAFORM_PROVIDER_TEMPLATE_VERSION}_linux_amd64.zip -d /usr/lib/custom-terraform-plugins && \
    unzip terraform-provider-archive_${TERRAFORM_PROVIDER_ARCHIVE_VERSION}_linux_amd64.zip -d /usr/lib/custom-terraform-plugins && \
    unzip terraform-provider-tls_${TERRAFORM_PROVIDER_TLS_VERSION}_linux_amd64.zip -d /usr/lib/custom-terraform-plugins && \
    unzip terraform-provider-external_${TERRAFORM_PROVIDER_EXTERNAL_VERSION}_linux_amd64.zip -d /usr/lib/custom-terraform-plugins && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/local/bin/ && \
    rm -rf *.zip && \
    curl -o /usr/local/bin/fly 'https://ci.rabbitmq.com//api/v1/cli?arch=amd64&platform=linux' && \
    chmod +x /usr/local/bin/fly && \
    curl -o /usr/local/bin/aws-nuke 'https://github.com/rebuy-de/aws-nuke/releases/download/v2.10.0/aws-nuke-v2.10.0-linux-amd64' -sL && \
    chmod +x /usr/local/bin/aws-nuke

RUN npm install -g serverless@${SLS_VERSION}

CMD ["/bin/sh"]
